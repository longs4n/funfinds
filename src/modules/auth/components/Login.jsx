import React, {useState} from 'react';
import "../../../assets/css/login.css"
import Button from "react-bootstrap/Button";
import {login} from "../core/request";
import {useAuth} from "./Auth";
import {Image} from "react-bootstrap";
import logo from '../../../assets/images/logo1.png'

const Login = () => {

    const [username, setUsername] = useState("");
    const [pass, setPass] = useState("");
    const [error, setError] = useState("");
    const {saveAuth} = useAuth();

    const onLogin = (event) => {
        event.preventDefault();
        login(username,pass).then((response) => {
            setError("");
            saveAuth(response.data.token)
        }).catch((error) => {
            setError(error.response.data.message);
        })
    }


    return (
        <div className="Auth-form-container">
            <form className="Auth-form">
                <div className="Auth-form-content">
                    <div className="text-center mb-3">
                        <Image width="128px" src={logo} />
                    </div>
                    <h3 className="Auth-form-title">Login</h3>
                    <div className="form-group mt-3">
                        <label>Username</label>
                        <input
                            type="text"
                            className="form-control mt-1"
                            placeholder="Enter username"
                            onChange={(e) => setUsername(e.target.value)}
                        />
                    </div>
                    <div className="form-group mt-3">
                        <label>Password</label>
                        <input
                            type="password"
                            className="form-control mt-1"
                            placeholder="Enter password"
                            onChange={(e) => setPass(e.target.value)}
                        />
                    </div>
                    <div className="d-grid gap-2 mt-3">
                        <Button onClick={onLogin} className="btn btn-dark px-5 mb-5 w-100">Login</Button>
                    </div>
                    <div className="text-center text-danger">{error}</div>
                    <div id="emailHelp" className="form-text text-center mb-5 text-dark">Not
                        Registered? <a href="#" className="text-dark fw-bold"> Create an
                            Account</a>
                    </div>
                </div>
            </form>
        </div>
    )
}
export default Login;