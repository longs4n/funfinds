import Card from 'react-bootstrap/Card';
import {Col, Row} from "react-bootstrap";
import slide1 from '../../../assets//images/slide1.jpg'
import slide2 from '../../../assets//images/slide2.jpg'
import slide3 from '../../../assets//images/slide3.jpg'


function CardBlog() {
    return (
        <>
        <div className="text-center  mt-4">
            <h2>Section Blog</h2>
        </div>
        <div className="container">
            <Row className="g-4 mt-4">
                <Col className="col-lg-6">
                    <Card>
                        <Card.Img variant="top" src={slide1}/>
                        <Card.Body>
                            <Card.Title>Card title</Card.Title>
                            <Card.Text>
                                This is a longer card with supporting text below as a natural
                                lead-in to additional content. This content is a little bit
                                longer.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
                <Col className="col-lg-6">
                    <Card>
                        <Card.Img variant="top" src={slide2}/>
                        <Card.Body>
                            <Card.Title>Card title</Card.Title>
                            <Card.Text>
                                This is a longer card with supporting text below as a natural
                                lead-in to additional content. This content is a little bit
                                longer.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
                <Col className="col-lg-12">
                    <Card>
                        <Card.Img variant="top" src={slide3}/>
                        <Card.Body>
                            <Card.Title>Card title</Card.Title>
                            <Card.Text>
                                This is a longer card with supporting text below as a natural
                                lead-in to additional content. This content is a little bit
                                longer.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </div>
        </>
    );
}

export default CardBlog;