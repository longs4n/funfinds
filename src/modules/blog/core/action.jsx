
function useArrayUtil(params) {
    const numbersOne = [1, 2, 3];
    const numbersTwo = [4, 5, 6];
    const numbersCombined = [...numbersTwo, ...numbersOne];

    function multix(value = 10) {
        return numbersCombined.map(num => num * value)
    }

    return {
        numbersOne,
        numbersTwo,
        numbersCombined,
        multix
    }
}


export { useArrayUtil }