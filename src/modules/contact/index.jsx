import ContactForm from "./core/ContactForm";

function ContactUsPage({ props }) {
    return <div>
        <ContactForm/>
    </div>
}

export default ContactUsPage;