import {useParams} from "react-router-dom";
import {useEffect, useState} from "react";
import {getProductById} from "../core/request";
import Card from "react-bootstrap/Card";

const ProductDetail = () => {
    const {id} = useParams();
    const [product, setProduct] = useState({});

    useEffect(() => {
        getProductById(id).then((response) => {
            setProduct(response.data);
        })
    }, [id]);
    if (!product.id) {
        return <div className="text-center fs-6">
            Loading...
        </div>
    }
    return <div className="container-fluid d-flex justify-content-center">
        <div className="row">
            <Card className="col-md-6 py-4 mt-5">
                <Card.Img img className="w-100" src={product.thumbnail} alt="__"/>
            </Card>
            <Card className="col-md-6 d-flex align-items-center mt-5 py-4">
                <Card.Body>
                    <Card.Text>
                        <div className="mb-3">
                            <h4>{product.title}</h4>
                        </div>
                        <div className="mb-3">
                            <h5>Description: {product.description}</h5>
                        </div>
                        <div className="mb-3">
                            <h5>Price:
                                <span className="text-danger">${product.price}  </span>
                            </h5>
                        </div>
                        <div className="mb-3">
                            <h5>Rating: {product.rating}</h5>
                        </div>
                        <div className="mb-3">
                            <h5>Brand: {product.brand}</h5>
                        </div>
                        <div className="mb-3">
                            <h5>Stock: {product.stock}</h5>
                        </div>
                    </Card.Text>
                </Card.Body>
            </Card>
        </div>
    </div>
}
export default ProductDetail;