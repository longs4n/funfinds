import {useState} from "react";
import {Pagination} from "react-bootstrap";


const PaginationBasic = ({total, onChangePage}) => {
    const [active, setActive] = useState(1);

    let items = [];
    for (let number = 1; number <= total; number++) {
        items.push(
            <Pagination.Item className="paginationItemStyle" onClick={() => {
                setActive(number);
                onChangePage && onChangePage(number);
            }} key={number} active={number === active}>
                {number}
            </Pagination.Item>
        );
    }
    return <Pagination size="md">{items}</Pagination>
}
export default PaginationBasic;