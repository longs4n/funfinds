import {useEffect, useState} from "react";
import {getListProduct, searchProduct, getCategories, getListProductByCategory} from "../core/request";
import {useNavigate} from "react-router-dom";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import {Badge} from "react-bootstrap";
import PaginationBasic from './Pagination';
import Card from "react-bootstrap/Card";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faStar} from "@fortawesome/free-regular-svg-icons";

const ListProducts = () => {

    const [products, setProducts] = useState([]);
    const [active, setActive] = useState("All");
    const [categories, setCategories] = useState([]);
    const [searchText, setSearchText] = useState("");
    const [total, setTotal] = useState(0);
    const navigate = useNavigate();

    const limit = 8;

    useEffect(() => {
        getListProduct(limit).then((response) => {
            setProducts(response.data.products);
            setTotal(response.data.total)
        });

        getCategories().then((response) => {
            setCategories(response.data);
        })
    }, []);

    useEffect(() => {
        active === "All" ?
            getListProduct(limit).then((response) => {
                setProducts(response.data.products);
                setTotal(response.data.total);
            })
            : getListProductByCategory(limit, active).then((response) => {
                setProducts(response.data.products);
                setTotal(response.data.total);
            });
    }, [active]);

    const onSearch = () => {
        searchProduct({q: searchText}).then((response) => {
            setProducts(response.data.products);
        });
    }

    return (
        <>npm
            <div className="container col-xl-4 py-4 pt-4">
                <Form className="d-flex px-4">
                    <Form.Control
                        type="search"
                        placeholder="Search"
                        className="me-2"
                        aria-label="Search"
                        onChange={(e) => setSearchText(e.target.value)}
                    />
                    <Button className="me-5" variant="dark" size="lg" onClick={onSearch}>Search</Button>
                </Form>
            </div>
            <div className="container d-flex justify-content-center align-items-center" style={{cursor:'pointer'}}>
                <div className="d-flex flex-wrap w-100 mx-4 pt-3 pb-3">
                    <Badge onClick={() => setActive("All")} className="mb-4 px-4 py-2 fs-6 mx-2"
                           bg={active === "All" ? "dark" : "secondary"}>All</Badge>
                    {categories.map((item, index) =>
                        <Badge onClick={() => setActive(item)} key={index} className="mb-4 px-4 py-2 fs-6 mx-2"
                               bg={active === item ? "dark" : "secondary"}>{item}</Badge>)}
                </div>
            </div>
            <div className="container d-flex flex-wrap justify-content-center mt-2">
                {products.length === 0 && <div>
                    Not Found
                </div>}
                {products.map((product, index) => {
                    return(
                        <Card className="mx-2 my-2" style={{width:"18rem"}}>
                            <div onClick={() => navigate(`/product/${product.id}`)}
                                 style={{}} key={index}>
                                <Card.Img img style={{
                                    width:"100%",
                                    height:"200px",
                                    borderBottom:"solid",
                                    borderColor:"whitesmoke"
                                }} className="" src={product.thumbnail} alt="__"/>
                                        <div className="my-3 ms-4">
                                                <h6>{product.title}</h6>
                                                <h6><FontAwesomeIcon icon={faStar}/> {product.rating}</h6>
                                                <h6><span className="text-danger"> ${product.price}</span></h6>
                                                <h6>{product.brand}</h6>
                                        </div>
                            </div>
                        </Card>
                    )
                })}
            </div>
            <div className="d-flex justify-content-center my-3">
                <PaginationBasic onChangePage={(page) => {
                    getListProduct(limit, (page - 1) * limit).then((response) => {
                        setProducts(response.data.products);
                        setTotal(response.data.total);
                    });
                }} total={total / limit}/>
            </div>
        </>
    )
}
export default ListProducts;