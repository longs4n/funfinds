import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Button from 'react-bootstrap/Button';
import {useAuth} from "../../auth/components/Auth";
import {Link} from "react-router-dom";
import {faRightFromBracket} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {Image} from "react-bootstrap";
import logo from '../../../assets/images/logo2.png'

function Header() {
    const {logout} = useAuth();
    return (
        <Navbar expand="lg" bg="dark" data-bs-theme="dark">
            <Container>
                <Navbar.Brand  className="fw-bold" as={Link} to="/">
                    <Image style={{width:'100px'}} src={logo}/>
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ms-auto">
                        <Nav.Link className="fw-bold" as={Link} to="/">Home</Nav.Link>
                        <Nav.Link className="fw-bold" as={Link} to="/product">Product</Nav.Link>
                        <Nav.Link className="fw-bold" as={Link} to="/blog">Blog</Nav.Link>
                        <Nav.Link className="fw-bold" as={Link} to="/contact">Contact</Nav.Link>
                        <NavDropdown className="me-5 fw-bold" title="Dropdown" id="basic-nav-dropdown">
                            <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
                            <NavDropdown.Item href="#action/3.2">
                                Another action
                            </NavDropdown.Item>
                            <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
                            <NavDropdown.Divider />
                            <NavDropdown.Item href="#action/3.4">
                                Separated link
                            </NavDropdown.Item>
                        </NavDropdown>
                    </Nav>
                    <Button  variant="light" className="fw-bold" size="lg"
                            onClick={() => {
                                logout()
                            }}>
                        <FontAwesomeIcon icon={faRightFromBracket} />
                    </Button>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}

export default Header;