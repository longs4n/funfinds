import ControlledCarousel from "./components/Slide";
import Card from "../blog/core/Card";

function HomePage() {

    return < >
        <div className="text-center">
            <ControlledCarousel/>
            <Card/>
        </div>
    </>
}
    export default HomePage;